import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Route, hashHistory, IndexRoute } from 'react-router'

/* Libs Stylesheet */
require('./assets/libs/bootstrap/css/bootstrap.min.css')
require('./assets/libs/fontawesome/css/font-awesome.css')


/* Stylesheets*/
require('./assets/css/style.css')
require('./assets/css/style-responsive.css')


/* React App */
import App from '../components/App'

/* Components */
import ExampleContent from '../components/ExampleContent'
import TablePage from '../components/modules/tablePage/tablePage'


const app = document.getElementById('app');

ReactDOM.render(
  <Router history={hashHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={ExampleContent}/>
      <Route path="/table" component={TablePage}/>
    </Route>
  </Router>,
app)
