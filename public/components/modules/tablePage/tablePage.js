import React, { Component } from 'react'

class TablePage extends Component{
  constructor(){
    super();
    this.state = {
      error: null,
      data : null,
      item: null,
    };
    fetch('http://localhost:8888/api/customers')
      .then((response) => response.json())
      .then((data)=>{
        this.setState({data:data})
      })
      .catch((error)=>{
        this.setState({error:error})
      });
  }
  render(){
    let {data, error, item} = this.state;

    if (error != null){
      return <p> There is something wrog </p>
    }
    if (data == null){
      return <p> Still loading </p>
    }
    let resultTable = data.map(function(item){
      return(
        <tr>
          <td>{item.ContactName}</td>
          <td>{item.ContactTitle}</td>
          <td>{item.Country}</td>
        </tr>
      )
    })
    return(
      <table className="table table-hover">
        <thead>
          <tr>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Email</th>
          </tr>
        </thead>
        <tbody>
          {resultTable}
        </tbody>
      </table>
    )
  }
}

export default TablePage
