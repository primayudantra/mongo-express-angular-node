import React, { Component } from 'react';

class ContentPage extends Component{
  render(){
    return(
      <div className="content-page">
        <div className="content">
          {this.props.children}
        </div>
      </div>
    )
  }
}

export default ContentPage
