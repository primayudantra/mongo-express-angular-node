import React, { Component } from 'react'
import { Link } from 'react-router'

class MenuList extends Component{
  render(){
    return(
      <div id="sidebar-menu">
        <ul>
          <li> <Link to="/"><i className='fa fa-home'></i> <span>Dashboard</span> </Link> </li>
          <li> <Link to="table"><i className='fa fa-table'></i> <span>Table Data</span></Link> </li>
        </ul>
      </div>
    )
  }
}

export default MenuList
