import React, { Component } from 'react'

import MenuList from './sideBarMenu'


class SideBar extends Component {
  render(){
    return(
      <div className="left side-menu">
        <MenuList/>
      </div>
    )
  }
}

export default SideBar
