import React, { Component } from 'react'

class HeaderBar extends Component{
  render(){
    return(
        <div className="topbar">
            <div className="topbar-left">
                <div className="logo">
                    <h1><a href="#"><img src="assets/img/logo.png" alt="Logo"/></a></h1>
                </div>
                <button className="button-menu-mobile open-left">
                <i className="fa fa-bars"></i>
                </button>
            </div>
            <div className="navbar navbar-default" role="navigation">
                <div className="container">
                    <div className="navbar-collapse2">
                        <ul className="nav navbar-nav hidden-xs">
                            <li className="dropdown">
                                <a href="#" className="dropdown-toggle" data-toggle="dropdown"><i className="icon-th"></i></a>
                                <div className="dropdown-menu grid-dropdown">
                                    <div className="row stacked">
                                        <div className="col-xs-4">
                                            <a href="javascript:;" data-app="notes-app" data-status="active"><i className="icon-edit"></i>Notes</a>
                                        </div>
                                        <div className="col-xs-4">
                                            <a href="javascript:;" data-app="todo-app" data-status="active"><i className="icon-check"></i>Todo List</a>
                                        </div>
                                        <div className="col-xs-4">
                                            <a href="javascript:;" data-app="calc" data-status="inactive"><i className="fa fa-calculator"></i>Calculator</a>
                                        </div>
                                    </div>
                                    <div className="row stacked">
                                        <div className="col-xs-4">
                                            <a href="javascript:;" data-app="weather-widget" data-status="active"><i className="icon-cloud-3"></i>Weather</a>
                                        </div>
                                        <div className="col-xs-4">
                                            <a href="javascript:;" data-app="calendar-widget2" data-status="active"><i className="icon-calendar"></i>Calendar</a>
                                        </div>
                                        <div className="col-xs-4">
                                            <a href="javascript:;" data-app="stock-app" data-status="inactive"><i className="icon-chart-line"></i>Stocks</a>
                                        </div>
                                    </div>
                                    <div className="clearfix"></div>
                                </div>
                            </li>
                            <li className="language_bar dropdown hidden-xs">
                                <a href="#" className="dropdown-toggle" data-toggle="dropdown">English (US) <i className="fa fa-caret-down"></i></a>
                                <ul className="dropdown-menu pull-right">
                                    <li><a href="#">German</a></li>
                                    <li><a href="#">French</a></li>
                                    <li><a href="#">Italian</a></li>
                                    <li><a href="#">Spanish</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul className="nav navbar-nav navbar-right top-navbar">
                            <li className="dropdown iconify hide-phone">
                                <a href="#" className="dropdown-toggle" data-toggle="dropdown"><i className="fa fa-globe"></i><span className="label label-danger absolute">4</span></a>
                                <ul className="dropdown-menu dropdown-message">
                                    <li className="dropdown-header notif-header"><i className="icon-bell-2"></i> New Notifications<a className="pull-right" href="#"><i className="fa fa-cog"></i></a></li>
                                    <li className="unread">
                                        <a href="#">
                                            <p><strong>John Doe</strong> Uploaded a photo <strong>&#34;DSC000254.jpg&#34;</strong>
                                                <br /><i>2 minutes ago</i>
                                            </p>
                                        </a>
                                    </li>
                                    <li className="unread">
                                        <a href="#">
                                            <p><strong>John Doe</strong> Created an photo album  <strong>&#34;Fappening&#34;</strong>
                                                <br /><i>8 minutes ago</i>
                                            </p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <p><strong>John Malkovich</strong> Added 3 products
                                                <br /><i>3 hours ago</i>
                                            </p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <p><strong>Sonata Arctica</strong> Send you a message <strong>&#34;Lorem ipsum dolor...&#34;</strong>
                                                <br /><i>12 hours ago</i>
                                            </p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <p><strong>Johnny Depp</strong> Updated his avatar
                                                <br /><i>Yesterday</i>
                                            </p>
                                        </a>
                                    </li>
                                    <li className="dropdown-footer">
                                        <div className="btn-group btn-group-justified">
                                            <div className="btn-group">
                                                <a href="#" className="btn btn-sm btn-primary"><i className="icon-ccw-1"></i> Refresh</a>
                                            </div>
                                            <div className="btn-group">
                                                <a href="#" className="btn btn-sm btn-danger"><i className="icon-trash-3"></i> Clear All</a>
                                            </div>
                                            <div className="btn-group">
                                                <a href="#" className="btn btn-sm btn-success">See All <i className="icon-right-open-2"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li className="dropdown iconify hide-phone">
                                <a href="#" className="dropdown-toggle" data-toggle="dropdown"><i className="fa fa-envelope"></i><span className="label label-danger absolute">3</span></a>
                                <ul className="dropdown-menu dropdown-message">
                                    <li className="dropdown-header notif-header"><i className="icon-mail-2"></i> New Messages</li>
                                    <li className="unread">
                                        <a href="#" className="clearfix">
                                            <img src="images/users/chat/2.jpg" className="xs-avatar ava-dropdown" alt="Avatar"/>
                                            <strong>John Doe</strong><i className="pull-right msg-time">5 minutes ago</i><br />
                                            <p>Duis autem vel eum iriure dolor in hendrerit ...</p>
                                        </a>
                                    </li>
                                    <li className="unread">
                                        <a href="#" className="clearfix">
                                            <img src="images/users/chat/1.jpg" className="xs-avatar ava-dropdown" alt="Avatar"/>
                                            <strong>Sandra Kraken</strong><i className="pull-right msg-time">22 minutes ago</i><br />
                                            <p>Duis autem vel eum iriure dolor in hendrerit ...</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" className="clearfix">
                                            <img src="images/users/chat/3.jpg" className="xs-avatar ava-dropdown" alt="Avatar"/>
                                            <strong>Zoey Lombardo</strong><i className="pull-right msg-time">41 minutes ago</i><br />
                                            <p>Duis autem vel eum iriure dolor in hendrerit ...</p>
                                        </a>
                                    </li>
                                    <li className="dropdown-footer"><div className=""><a href="#" className="btn btn-sm btn-block btn-primary"><i className="fa fa-share"></i> See all messages</a></div></li>
                                </ul>
                            </li>
                            <li className="dropdown topbar-profile">
                                <a href="#" className="dropdown-toggle" data-toggle="dropdown"><span className="rounded-image topbar-profile-image"><img src="images/users/user-35.jpg"/></span> Jane <strong>Doe</strong> <i className="fa fa-caret-down"></i></a>
                                <ul className="dropdown-menu">
                                    <li><a href="#">My Profile</a></li>
                                    <li><a href="#">Change Password</a></li>
                                    <li><a href="#">Account Setting</a></li>
                                    <li className="divider"></li>
                                    <li><a href="#"><i className="icon-help-2"></i> Help</a></li>
                                    <li><a href="lockscreen.html"><i className="icon-lock-1"></i> Lock me</a></li>
                                    <li><a className="md-trigger" data-modal="logout-modal"><i className="icon-logout-1"></i> Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
  }
}

export default HeaderBar
