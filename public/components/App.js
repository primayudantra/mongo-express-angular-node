import React, { Component } from 'react'
import { Link } from 'react-router'

/* Modules */
import HeaderBar from './modules/app/headerBar.js'
import SideBar from './modules/app/sideBar.js'
import ContentPage from './modules/app/contentPage.js'

class App extends Component{
  render(){
    return(
      <div id="wrapper">
        <HeaderBar/>
        <SideBar/>
        <div className="content-page">
          <div className="content">
            {this.props.children}
          </div>
        </div>
      </div>
    )
  }
}

export default App
