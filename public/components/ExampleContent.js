import React, { Component } from 'react'

class ExampleContent extends Component{
  constructor(){
    super();
    this.state = {
      error: null,
      data: null
    };
    fetch('http://localhost:8888/api/customers')
      .then((response) => response.json())
      .then((data)=>{
        this.setState({data:data})
        console.log(data)
      })
      .catch((error)=>{
        this.setState({error:error})
      });
  }

  render(){
    let { data,error } = this.state;
    if (error != null){
      return <p> There is something wrong </p>;
    }
    if (data == null){
      return <p>Still Waiting </p>;
    }
    var xxx = data.map(function(datas){
      return(
        <ul>
          <li>{datas.ContactName}</li>
        </ul>
      )
    })
    return(
      <div>{xxx}</div>
    )
  }
}

export default ExampleContent
