var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var StaticSiteGeneratorPlugin = require('static-site-generator-webpack-plugin');

module.exports = {
  devtool: 'inline-source-map',
  entry:[
      'webpack-hot-middleware/client',
      './public/client/client.js'
  ],
  output: {
      path: require("path").resolve("./public/dist"),
      filename: 'bundle.js',
      publicPath: '/',
      libraryTarget: 'umd'
  },
  plugins:[
      new webpack.optimize.OccurrenceOrderPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoErrorsPlugin(),
      new ExtractTextPlugin('style.css',{allChunks: true})
  ],
  module:{
    loaders:[
      {
        test: /\.js$/, loader: 'babel-loader', exclude: /node-modules/,
        query:{
            presets:[
              'react',
              'es2015',
              'react-hmre'
            ]
        }
      },
      {
        test: /\.css$/,
        loader: "style-loader!css-loader"
      },
      {
        test: /\.(png|jpg|jpeg|gif|woff|eot|ttf|svg)$/,
        loader: 'url-loader?limit=8192'
      },
      
      /* This Regex Loader for Font Awesome*/
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "url-loader?limit=10000&minetype=application/font-woff"
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "file-loader"
      }
    ]
  }

}
