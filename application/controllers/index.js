'use strict';

/** -------------------------------
*
*   LIST LIBRARY
*
------------------------------- **/

let Settings = require('settings'),
    mongojs = require('mongojs');

/** -------------------------------
*
*   DB CONFIG
*
------------------------------- **/

let dbConfig = require(`${__dirname}/../../config/database`);

const DatabaseName = dbConfig.database.dbName;
const DatabaseCollections = dbConfig.database.collections;

/* MONGO CONNECTION */
let db = mongojs(DatabaseName, DatabaseCollections)


/** -------------------------------
*
*   LIST FUNCTION CONTROLLER
*
------------------------------- **/
module.exports = {
  Index: function(req,res,next){
    res.send("Welcome to API")
  },

  Mongo: function(req,res,next){
    db.collection('orders').find({}, function(err,data){
      res.json(data)
    })
  },

  Products: function(req, res, next){
    db.collection('products').find({}, function(err, data){
      res.json(data)
    })
  },

  Region: function(req,res,next){
    db.collection('region').find({}, function(err, data){
      res.json(data)
    })
  },

  Customers: function(req,res,next){
    db.collection('customers').find({}, function(err, data){
      res.json(data)
    })
  }
}
