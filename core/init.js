'use strict';

/** -------------------------------
*
*   LIST LIBRARY
*
------------------------------- **/

let express    = require('express'),
    Pipa       = require('pipa'),
    Settings   = require('settings'),
    ejs        = require('ais-ejs-mate');


/** -------------------------------
*
*   PORT PATH (DEV/STAGING/PRODUCTION)
*
------------------------------- **/
let commonConfig = new Settings(`${__dirname}/../config/common`);

const app = express();

module.exports = function(){
  /** -------------------------------
  *
  *   DIRECTORY OF
  *   CONTROLLER AND ROUTER(PIPAJS SETUP)
  *
  ------------------------------- **/
  const controllersPath = `application/controllers`;
  const routesPath = `application/routes`;

  require(`${__dirname}/../app`)(app);

  /** -------------------------------
  *
  *   OPEN PORT AND RUNNING SERVER
  *
  ------------------------------- **/
  (new Pipa(app, routesPath, controllersPath)).open();
  app.listen(commonConfig.expressPort)

  console.log("---------------------------")
  console.log("SERVER RUNNING ON")
  console.log("SERVER: " + commonConfig.expressHost + " PORT: " + commonConfig.expressPort)
  console.log("@primayudantra")
  console.log("---------------------------")
}
