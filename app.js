'use strict'

/** -------------------------------
*
*   GLOBAL VARIABLE
*
------------------------------- **/

let express     = require('express'),
    bodyParser  = require('body-parser'),
    ejs         = require('ais-ejs-mate'),
    Settings    = require('settings'),
    path        = require('path'),
    Pipa        = require('pipa');

let webpack               = require('webpack'),
    webpackDevMiddleware  = require('webpack-dev-middleware'),
    webpackHotMiddleware  = require('webpack-hot-middleware');

const app = express();
const config = require('./webpack.config.js')
const compiler = webpack(config);

module.exports = function(app){
  /** -------------------------------
  *
  *   WEBPACK SETUP
  *
  ------------------------------- **/

  app.use(webpackDevMiddleware(compiler, {noInfo: true, publicPath: config.output.publicPath}))
  app.use(webpackHotMiddleware(compiler));



  /** -------------------------------
  *
  *   EXPRESS SETUP
  *
  ------------------------------- **/

  app.use(bodyParser.urlencoded({extended: true}))
  app.use(bodyParser.json({extended: true}))
  app.use(express.static(__dirname + '/public/client'));
  // app.use('*', function (req, res) { res.sendFile(path.resolve('public/client/index.html'))});
  app.engine('.html',ejs)
  app.set('view engine', 'html')

}
