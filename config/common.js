'use strict'

/** --------------------------------
*
*   PORT CONFIGURATION
*
-------------------------------- **/

module.exports = {

  common: {
    expressHost: '0.0.0.0',
    expressPort: '8888'
  },

  staging: {},

  production: {}
};
