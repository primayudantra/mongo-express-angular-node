'use strict';


/** --------------------------------
*
*   DATABASE CONFIGURATION
*
-------------------------------- **/

module.exports = {
  database:  {
    host : '127.0.0.1',
    port : '27017',
    dbName : 'dump-db',
    collections: ['orders','products','region','customers','categories','orderdetails']
  },

  mysql: {},

  postgresql: {},

};
